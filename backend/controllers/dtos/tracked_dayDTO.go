package dtos

type TrackedDayDTO struct {
	TrackedDayId int64
	Day          int64
	Confirmed    bool
	ChoiceId     int64
	Validated    float64
	Unvalidated  float64
	Missing      float64
}
