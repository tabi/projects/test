package controllers

import (
	"github.com/gin-gonic/gin"
	"main/databases"
	"main/models"
)

func UpsertClassifiedPeriod(c *gin.Context) {
	userID, _ := c.Get("userID")
	var classifiedPeriod models.ClassifiedPeriod
	var error = c.ShouldBindJSON(&classifiedPeriod)
	if error != nil {
		print(error.Error())
	}
	classifiedPeriod.UserId = userID.(int64)
	if databases.DB.Model(&classifiedPeriod).Where("uuid = ?", classifiedPeriod.Uuid).Updates(&classifiedPeriod).RowsAffected == 0 {
		databases.DB.Create(&classifiedPeriod)
	}
	c.JSON(200, classifiedPeriod)
}
