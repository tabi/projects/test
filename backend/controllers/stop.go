package controllers

import (
	"github.com/gin-gonic/gin"
	"main/databases"
	"main/models"
)

func UpsertStop(c *gin.Context) {
	userID, _ := c.Get("userID")
	var stop models.Stop
	var error = c.ShouldBindJSON(&stop)
	if error != nil {
		print(error.Error())
	}
	stop.UserId = userID.(int64)
	if databases.DB.Model(&stop).Where("uuid = ?", stop.Uuid).Updates(&stop).RowsAffected == 0 {
		databases.DB.Create(&stop)
	}
	c.JSON(200, stop)
}
