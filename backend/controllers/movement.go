package controllers

import (
	"github.com/gin-gonic/gin"
	"main/databases"
	"main/models"
)

func UpsertMovement(c *gin.Context) {
	userID, _ := c.Get("userID")
	var movement models.Movement
	var error = c.ShouldBindJSON(&movement)
	if error != nil {
		print(error.Error())
	}
	movement.UserId = userID.(int64)
	if databases.DB.Model(&movement).Where("uuid = ?", movement.Uuid).Updates(&movement).RowsAffected == 0 {
		databases.DB.Create(&movement)
	}
	c.JSON(200, movement)
}
