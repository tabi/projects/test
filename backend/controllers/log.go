package controllers

import (
	"main/databases"
	"main/models"

	"github.com/gin-gonic/gin"
)

// GetLatestLogs godoc
// @Summary      Gets latest logs of secureId
// @Description  Gets latest logs of secureId from database
// @Tags         logging
// @Accept       json
// @Produce      json
// @Param        secureId   path	int  true	"Secure ID"
// @Success      200  {array}   models.Log
// @Failure      400  {object}  models.ErrorLog
// @Failure      404  {object}  models.ErrorLog
// @Router       /logs/getLatest/{secureId}			[get]
func GetLatestLogs(c *gin.Context) {
	var logs []models.Log
	secureId := c.Param("secureId")

	databases.DB.Table("logs").Joins("JOIN devices ON devices.id = logs.device_id").Where("devices.secure_id = ?", secureId).Order("id DESC").Limit(10).Find(&logs)
	c.JSON(200, logs)
}

// LogTrackers godoc
// @Summary      Posts log of tracker
// @Description  Posts log of tracker to database
// @Tags         logging
// @Accept       json
// @Produce      json
// @Param        logs   body	models.Log  true	"Log"
// @Success      200  {object}   models.Log
// @Failure      400  {object}  models.ErrorLog
// @Failure      404  {object}  models.ErrorLog
// @Router       /logs			[post]
func LogTrackers(c *gin.Context) {
	var log models.Log
	c.ShouldBindJSON(&log)
	var device models.Device
	databases.DB.Find(&device, models.Device{SecureId: log.Device.SecureId})

	if device.SecureId != "" {
		log.DeviceID = int64(device.ID)
		log.Device = models.Device{}
	}

	databases.DB.Create(&log)
	c.JSON(200, log)
}

// SyncLogs godoc
// @Summary      Syncs logs
// @Description  Syncs logs
// @Tags         logging
// @Accept       json
// @Produce      json
// @Param        logs   body	models.Log  true	"Log"
// @Success      200  {object}   models.Device
// @Failure      400  {object}  models.ErrorLog
// @Failure      404  {object}  models.ErrorLog
// @Router       /logs/syncLogs			[post]
func SyncLogs(c *gin.Context) {
	var logs []models.Log
	c.ShouldBindJSON(&logs)
	var device models.Device

	for i, log := range logs {
		print(i)

		databases.DB.Find(&device, models.Device{SecureId: log.Device.SecureId})

		if device.SecureId != "" {
			log.DeviceID = int64(device.ID)
			log.Device = models.Device{}
		}

		databases.DB.Create(&log)
	}

	c.JSON(200, device)
}
