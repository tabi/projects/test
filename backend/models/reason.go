package models

import (
	"github.com/jinzhu/gorm"
)

type Reason struct {
	gorm.Model

	Name  string `json:"name"`
	Icon  string `json:"icon"`
	Color string `json:"color"`
	Key   string `json:"key"`
}
