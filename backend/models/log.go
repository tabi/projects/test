package models

import (
	"github.com/jinzhu/gorm"
)

type Log struct {
	gorm.Model

	Message     string
	Description string
	DeviceID    int64
	Device      Device
	Type        string
	DateTime    int64
}

// types
// - synced
// - clicked
// -
