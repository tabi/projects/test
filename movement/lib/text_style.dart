import 'package:flutter/material.dart';

import 'color_pallet.dart';

const textStyleSoho16Black = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 16,
  color: ColorPallet.darkTextColor,
);

const textStyleSoho20Black = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 20,
  color: ColorPallet.darkTextColor,
);

const textStyleSoho24 = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24,
  color: Colors.white,
);

const textStyleSoho24Black = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24,
  color: ColorPallet.darkTextColor,
);

const textStyleSoho36 = TextStyle(
  fontFamily: 'Soho Pro',
  fontWeight: FontWeight.w500,
  fontSize: 36,
  color: Colors.white,
);

const textStyleAkko24 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24,
  color: Colors.white,
);

const textStyleAkko24Blue = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 24,
  color: ColorPallet.primaryColor,
);

const textStyleAkko20 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 20,
  color: Colors.white,
);

const textStyleAkko18 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 18,
  color: Colors.white,
);

const textStyleAkko18Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w500,
  fontSize: 18,
  color: ColorPallet.darkTextColor,
);

const textStyleAkko18Blue = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 18,
  color: ColorPallet.primaryColor,
);

final textStyleAkko16 = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.white.withOpacity(0.7),
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleAkko16White = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.white,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleAkko16Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.black,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleAkko16Grey = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: ColorPallet.darkTextColor.withOpacity(0.5),
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleAkko16Blue = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: ColorPallet.primaryColor,
);

const textStyleAkko14Blue = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: ColorPallet.primaryColor,
);

final textStyleAkko16Hint = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: ColorPallet.darkTextColor.withOpacity(0.2),
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleAkko14Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 14,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleAkko12Black = TextStyle(
  fontFamily: 'Akko Pro',
  fontWeight: FontWeight.w400,
  fontSize: 12,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleSourceSans16Black = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleSourceSans16BlackHeavy = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w600,
  fontSize: 16,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans16Grey = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: ColorPallet.darkTextColor.withOpacity(0.5),
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleSourceSans16 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.white,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

const textStyleSourceSans18 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w600,
  fontSize: 18,
  color: ColorPallet.primaryColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);

final textStyleSourceSans16Oopacity = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w400,
  fontSize: 16,
  color: Colors.white.withOpacity(0.7),
  decoration: TextDecoration.underline,
);

const textStyleSourceSans20 = TextStyle(
  fontFamily: 'SourceSans',
  fontWeight: FontWeight.w600,
  fontSize: 20,
  color: ColorPallet.darkTextColor,
  decoration: TextDecoration.none,
  decorationThickness: 0,
);
