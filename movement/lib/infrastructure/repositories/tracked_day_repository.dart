import 'package:calendar_page/calendar_page.dart';
import 'package:movement/infrastructure/repositories/dtos/tracked_day_dto.dart';
import 'package:uuid/uuid.dart';

import 'database/database.dart';
import 'network/tracked_api.dart';

class TrackedDayRepository {
  final Database _database;
  final TrackedApi _trackedApi;

  TrackedDayRepository(this._database, this._trackedApi);

  Future<void> insertTrackedDay(int day) async {
    final dateTime = DateTime.now().add(Duration(days: day));
    final dateTimeStartOfDay = DateTime(dateTime.year, dateTime.month, dateTime.day);
    final trackedDay = TrackedDay(
      confirmed: false,
      date: dateTimeStartOfDay,
      uuid: Uuid().v4(),
      synced: false,
    );

    await _database.trackedDayDao.insertTrackedDay(trackedDay);
    await _trackedApi.insertTrackedDay(TrackedDayDTO(trackedDayUuid: trackedDay.uuid, day: dateTimeStartOfDay.millisecondsSinceEpoch));
  }

  Future<String?> getTrackedDayUuid(DateTime dateTime) async {
    final trackedDay = await _database.trackedDayDao.getTrackedDay(dateTime);
    return trackedDay?.uuid;
  }

  Future<List<TrackedDay>> getAllTrackedDay() {
    throw UnimplementedError();
  }

  Future<List<TrackedDay>> getTrackedDay() {
    throw UnimplementedError();
  }

  Future<bool> isWeekComplete() {
    return _database.trackedDayDao.allDaysCompleted();
  }

  Future<List<CalendarPageDayData>> getCalendarPageDayDataList() async {
    final trackedDays = await _database.trackedDayDao.getAllTrackedDays();
    return Future.wait(trackedDays.map((t) async {
      final classifiedPeriods = await _database.classifiedPeriodDao.getClassifiedPeriodsOnDay(t.date);
      final progress = _getDailyProgress(classifiedPeriods);
      return CalendarPageDayData(
        confirmed: t.confirmed,
        day: t.date,
        missing: progress['missing']!,
        unvalidated: progress['unvalidated']!,
        validated: progress['validated']!,
      );
    }).toList());
  }

  Future<bool> isInTrackedDays(DateTime dateTime) async {
    // final trackedDay = await _database.trackedDayDao.getTrackedDay(dateTime);
    // return trackedDay != null;

    final days = await _database.trackedDayDao.getAllTrackedDays();
    final newDateTime = DateTime(dateTime.year, dateTime.month, dateTime.day);

    var timetime = DateTime(days.last.date.year, days.last.date.month, days.last.date.day);
    if (newDateTime.isAfter(timetime)) {
      return false;
    }

    timetime = DateTime(days[0].date.year, days[0].date.month, days[0].date.day);
    if (newDateTime.isBefore(timetime)) {
      return false;
    }

    timetime = DateTime.now();
    if (newDateTime.isBefore(DateTime.now()) || newDateTime.isAtSameMomentAs(DateTime.now())) {
      return true;
    } else {
      return false;
    }
  }

  Future<CalendarPageDayData> getCalendarPageDayData(DateTime dateTime) async {
    final trackedDay = await _database.trackedDayDao.getTrackedDay(dateTime);
    final classifiedPeriods = await _database.classifiedPeriodDao.getClassifiedPeriodsOnDay(dateTime);
    final progress = _getDailyProgress(classifiedPeriods);
    return CalendarPageDayData(
      confirmed: trackedDay?.confirmed ?? false,
      day: dateTime,
      missing: progress['missing']!,
      unvalidated: progress['unvalidated']!,
      validated: progress['validated']!,
    );
  }

  Stream<CalendarPageDayData> streamCalendarPageData(DateTime dateTime) async* {
    final trackedDay = await _database.trackedDayDao.getTrackedDay(dateTime);
    yield* _database.classifiedPeriodDao.streamClassifiedPeriodsOnDay(dateTime).asyncMap((classifiedPeriods) {
      final progress = _getDailyProgress(classifiedPeriods);
      return CalendarPageDayData(
        confirmed: trackedDay?.confirmed ?? false,
        day: dateTime,
        missing: progress['missing']!,
        unvalidated: progress['unvalidated']!,
        validated: progress['validated']!,
      );
    });
  }

  Map<String, double> _getDailyProgress(List<ClassifiedPeriod> classifiedPeriods) {
    final results = <String, double>{};
    results['validated'] = 0.0;
    results['unvalidated'] = 0.0;
    results['missing'] = 86400.0;
    for (final classifiedPeriod in classifiedPeriods) {
      final durationInSeconds = (classifiedPeriod.endDate.millisecondsSinceEpoch - classifiedPeriod.startDate.millisecondsSinceEpoch) * 0.001;
      results['missing'] = results['missing']! - durationInSeconds;
      if (classifiedPeriod.confirmed) {
        results['validated'] = durationInSeconds + results['validated']!;
      } else {
        results['unvalidated'] = durationInSeconds + results['unvalidated']!;
      }
    }
    return results;
  }
}
