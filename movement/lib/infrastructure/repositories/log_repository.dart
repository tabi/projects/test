import '../../main.dart';
import '../../providers.dart';
import '../services/device_service.dart';
import 'database/database.dart';
import 'device_repository.dart';
import 'dtos/device_dto.dart';
import 'dtos/enums/log_type.dart';
import 'dtos/log_dto.dart';
import 'network/log_api.dart';

class LogRepository {
  final LogApi _logApi;
  final Database _database;
  final DeviceRepository _deviceRepository;
  final DeviceService _deviceService;

  LogRepository(this._logApi, this._database, this._deviceRepository, this._deviceService);

  Future postLogAsync(String message, String description, LogType type) async {
    await _deviceRepository.checkIfDeviceIsStored();
    final device = await (_database.devicesDao.getDeviceAsync() as Future<Device>);
    final logDTO = LogDTO(
      message: message,
      description: description,
      deviceDTO: DeviceDTO.fromDevice(device),
      type: type.toString(),
      datetime: DateTime.now().millisecondsSinceEpoch,
    );

    await _logApi.postLog(logDTO);
  }

  Future syncLogsToServer() async {
    final logs = await _database.logsDao.getUnSyncedLogs();
    final device = await _deviceService.getDeviceAsync();

    for (var log in logs) {
      log = log.copyWith(synced: true);
      await _database.logsDao.updateLog(log);
    }

    await _logApi.postLogs(LogDTO.fromList(logs, device!));
  }
}

Future<void> log(String message, dynamic description, LogType logType) async {
  final convertedDescripion = description is String ? description : '';
  final db = container.read(database);
  final log = Log(message: message, description: convertedDescripion, synced: false, type: logType.toString(), date: DateTime.now());

  // await db.logsDao.insertLog(log);
}

Future<void> logAuth(String description) async {
  final api = container.read(logApi);
  final deviceService = container.read(deviceServiceProvider);
  final device = await deviceService.getDeviceAsync();

  await api.postLogs(LogDTO.fromList(
      [Log(message: 'AuthNotifier::authenticate', description: description, synced: false, type: LogType.Flow.toString(), date: DateTime.now())], device!));
}
