import 'package:json_annotation/json_annotation.dart';

part 'tracked_day_dto.g.dart';

@JsonSerializable()
class TrackedDayDTO {
  String? trackedDayUuid;
  int? day;
  int? choiceId;
  bool? confirmed;
  double? validated;
  double? unvalidated;
  double? missing;

  TrackedDayDTO({
    this.trackedDayUuid,
    this.day,
    this.choiceId,
    this.confirmed,
    this.validated,
    this.unvalidated,
    this.missing,
  });

  factory TrackedDayDTO.fromMap(Map<String, dynamic> map) => _$TrackedDayDTOFromJson(map);

  Map<String, dynamic> toJson() => _$TrackedDayDTOToJson(this);
}
