import '../database/database.dart';
import '../dtos/parsed_response.dart';
import 'base_api.dart';

class SensorGeolocationApi extends BaseApi {
  SensorGeolocationApi(Database database) : super('syncing/', database);

  Future<ParsedResponse<SensorGeolocation>> syncSensorGeolocation(SensorGeolocation sensorGeolocation) async {
    final payload = sensorGeolocation.toJson();
    return this.getParsedResponse<SensorGeolocation, SensorGeolocation>(
      'AddGeoLocation',
      SensorGeolocation.fromJson,
      payload: payload,
    );
  }
}
