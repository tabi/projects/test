import 'database/database.dart';
import 'network/sensor_geolocation_api.dart';

class SensorRepository {
  final Database _database;
  final SensorGeolocationApi _sensorGeolocationApi;

  SensorRepository(this._database, this._sensorGeolocationApi);

  Future<int> insertSensorGeolocation(SensorGeolocation sensorGeolocation) async => _database.sensorGeolocationDao.insertSensorGeolocation(sensorGeolocation);

  Future<int> getSensorGeolocationCount() async => _database.sensorGeolocationDao.getSensorGeolocationCount();

  Future<void> syncSensorGeolocations() async {
    final unsycnedSensorGeolocations = await _database.sensorGeolocationDao.getUnsycnedSensorGeolocations();
    for (final unsynced in unsycnedSensorGeolocations) {
      final _response = await _sensorGeolocationApi.syncSensorGeolocation(unsynced);
      if (_response.isOk) await _database.sensorGeolocationDao.replaceSensorGeolocation(unsynced.copyWith(synced: true));
    }
  }
}
