import 'package:drift/drift.dart';
import 'package:uuid/uuid.dart';

import '../database.dart';
import '../tables/classified_period_table.dart';
import '../tables/movement_table.dart';
import '../tables/stop_table.dart';

part 'classified_period_dao.g.dart';

@DriftAccessor(tables: [ClassifiedPeriods, Movements, Stops])
class ClassifiedPeriodDao extends DatabaseAccessor<Database> with _$ClassifiedPeriodDaoMixin {
  ClassifiedPeriodDao(Database db) : super(db);

  Future<List<ClassifiedPeriod>> getUnsyncedClassifiedPeriods() async => (select(classifiedPeriods)..where((c) => c.synced.equals(false))).get();

  Future<List<ClassifiedPeriod>> getClassifiedPeriods() async {
    return (select(classifiedPeriods)
          ..where((c) => c.deletedOn.isNull())
          ..orderBy([
            (c) => OrderingTerm(expression: c.startDate),
          ]))
        .get();
  }

  Future<ClassifiedPeriod?> getLastClassifiedPeriod() async {
    final classifiedPeriods = await getClassifiedPeriods();
    return classifiedPeriods.isEmpty ? null : classifiedPeriods.last;
  }

  Future<bool> isStop(ClassifiedPeriod classifiedPeriod) async {
    return (await (select(classifiedPeriods).join([
          innerJoin(stops, stops.classifiedPeriodUuid.equalsExp(classifiedPeriods.uuid)),
        ])
              ..where(classifiedPeriods.uuid.equals(classifiedPeriod.uuid)))
            .map((row) => true)
            .getSingleOrNull()) ??
        false;
  }

  Future<void> addStop(String? trackedDayUuid, SensorGeolocation sensorGeolocation) async {
    final classifiedPeriodUuid = await _addClassifiedPeriod(trackedDayUuid, sensorGeolocation);
    await into(stops).insert(StopsCompanion.insert(classifiedPeriodUuid: classifiedPeriodUuid));
  }

  Future<void> addMovement(String? trackedDayUuid, SensorGeolocation sensorGeolocation) async {
    final classifiedPeriodUuid = await _addClassifiedPeriod(trackedDayUuid, sensorGeolocation);
    await into(movements).insert(MovementsCompanion.insert(classifiedPeriodUuid: classifiedPeriodUuid));
  }

  Future<String> _addClassifiedPeriod(String? trackedDayUuid, SensorGeolocation sensorGeolocation) async {
    return (await into(classifiedPeriods).insertReturning(
      ClassifiedPeriodsCompanion.insert(
        uuid: Value(Uuid().v4()),
        origin: Value('classifier_insert'),
        trackedDayUuid: Value(trackedDayUuid),
        startDate: sensorGeolocation.createdOn,
        endDate: sensorGeolocation.createdOn,
        createdOn: DateTime.now(),
        synced: Value(false),
      ),
    )).uuid;
  }

  Future<void> replaceClassifiedPeriod(ClassifiedPeriod classifiedPeriod) async => await update(classifiedPeriods).replace(classifiedPeriod);

  Future<List<ClassifiedPeriod>> getClassifiedPeriodsOnDay(DateTime dateTime) async {
    //TODO: fix timezone issue here
    return (select(classifiedPeriods)
          ..where((c) => c.startDate.year.equals(dateTime.year))
          ..where((c) => c.startDate.month.equals(dateTime.month))
          ..where((c) => c.startDate.day.equals(dateTime.day))
          ..where((c) => c.deletedOn.isNull()))
        .get();
  }

  Stream<List<ClassifiedPeriod>> streamClassifiedPeriodsOnDay(DateTime dateTime) async* {
    //TODO: fix timezone issue here
    yield* (select(classifiedPeriods)
          ..where((c) => c.startDate.year.equals(dateTime.year))
          ..where((c) => c.startDate.month.equals(dateTime.month))
          ..where((c) => c.startDate.day.equals(dateTime.day))
          ..where((c) => c.deletedOn.isNull()))
        .watch();
  }
}
