import 'dart:convert';
import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../main.dart';
import '../../providers.dart';
import '../repositories/dtos/enums/log_type.dart';
import '../repositories/log_repository.dart';

Future createCallBackIsolate() async {
  WidgetsFlutterBinding.ensureInitialized();
  await startMethodChannel();
}

class ForegroundService {
  Future startForegroundServiceAsync() async {
    const channel = MethodChannel('com.example/background_service');
    final callbackHandle = PluginUtilities.getCallbackHandle(createCallBackIsolate)!;

    try {
      await channel.invokeMethod<void>('startService', callbackHandle.toRawHandle());
      await startMethodChannel();
    } catch (error) {
      await log('ForegroundService::startForegroundServiceAsync Error', error.toString(), LogType.Error);
    }
  }

  Future stopForegroundServiceAsync() async {
    try {
      await const MethodChannel('com.example.flutter/foreground_service').invokeMethod<void>('stopForegroundService');
    } catch (error) {
      await log('ForegroundService::stopForegroundServiceAsync Error', error.toString(), LogType.Error);
    }
  }

  Future<bool> isForegroundServiceRunningAsync() async {
    try {
      final available = await const MethodChannel('com.example.flutter/foreground_service').invokeMethod<bool?>('isForegroundServiceRunning');

      if (available == null) {
        return false;
      }

      return available;
    } catch (error) {
      await log('ForegroundService::isForegroundServiceRunningAsync Error', error.toString(), LogType.Error);
      return false;
    }
  }
}

Future startMethodChannel() async {
  try {
    const MethodChannel('com.example.flutter/location_info').setMethodCallHandler((call) async {
      await log('ForegroundService::startMethodChannel', 'method: ${call.method}}, arguments: ${call.arguments}', LogType.Flow);
      if (call.method == 'save') {
        final geolocationMap = jsonDecode(call.arguments as String) as Map<String, dynamic>;

        var delayInMiliseconds = 0;
        switch (geolocationMap['sensorType'] as String) {
          case 'normal':
            delayInMiliseconds = 0 + Random().nextInt(500);
            break;
          case 'gps':
            delayInMiliseconds = 600 + Random().nextInt(500);
            break;
          case 'fused':
            delayInMiliseconds = 1200 + Random().nextInt(500);
            break;
          case 'balanced':
            delayInMiliseconds = 1800 + Random().nextInt(500);
            break;
        }
        Future.delayed(Duration(milliseconds: delayInMiliseconds), () async {
          final stopClassifierNotifier = container.read(stopClassifierProvider);
          final deviceService = container.read(deviceServiceProvider);

          final batteryLevel = await deviceService.getBatteryLevelAsync();

          await stopClassifierNotifier.addSensorGeolocation(
              latitude: geolocationMap['lat'] as double,
              longitude: geolocationMap['lon'] as double,
              accuracy: geolocationMap['accuracy'] as double,
              altitude: geolocationMap['altitude'] as double,
              bearing: geolocationMap['bearing'] as double,
              speed: geolocationMap['speed'] as double,
              sensorType: geolocationMap['sensorType'] as String,
              provider: geolocationMap['providers'] as String,
              batteryLevel: batteryLevel ?? 0);
        });

        try {
          await container.read(sensorRepository).syncSensorGeolocations();
          await container.read(classifiedPeriodRepository).syncClassifiedPeriods();
          await container.read(stopRepository).syncStops();
          await container.read(movementRepository).syncMovements();
          await container.read(manualGeolocationRepository).syncManualGeolocation();
          await container.read(googleMapsRepository).syncGoogleMapsData();
        } catch (error) {
          print(error.toString());
        }
      }
    });
  } catch (error) {
    await log('ForegroundService::startMethodChannel Error', error.toString(), LogType.Error);
  }
}
