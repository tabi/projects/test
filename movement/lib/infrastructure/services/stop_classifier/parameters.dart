DateTime getReferenceDateMin() => DateTime.now().subtract(const Duration(minutes: 30));

DateTime getReferenceDateMax() => DateTime.now().subtract(const Duration(minutes: 1));

const int distanceTreshholdInMeters = 50;

const int noiseTreshhold = 50;
