import 'parameters.dart';

bool _hasLowAccuracy(double accuracy) => accuracy > noiseTreshhold; //TODO: Needs to be adapted to phones where accuracy is always low (e.g. 40 OR best 50%)

bool isNoise(double accuracy) {
  if (_hasLowAccuracy(accuracy)) return true;
  return false;
}
