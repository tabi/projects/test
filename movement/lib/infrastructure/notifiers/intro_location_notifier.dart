import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import '../repositories/dtos/enums/log_type.dart';
import '../repositories/log_repository.dart';

class IntroLocationNotifier extends ChangeNotifier {
  bool isGranted = false;
  bool isPressed = false;

  Future<void> checkPermission() async {
    final access = await Geolocator.checkPermission();
    await log('IntroductionPage::askLocationPermission', 'access: $access', LogType.Flow);
    switch (access) {
      case LocationPermission.deniedForever:
        isGranted = false;
        break;
      case LocationPermission.denied:
        isGranted = false;
        break;
      case LocationPermission.always:
        isGranted = true;
        break;
      case LocationPermission.whileInUse:
        isGranted = true;
        break;
      default:
        isGranted = false;
        break;
    }

    isPressed = true;
    notifyListeners();
  }

  Future<void> askLocationPermission() async {
    await Geolocator.requestPermission();
  }
}
