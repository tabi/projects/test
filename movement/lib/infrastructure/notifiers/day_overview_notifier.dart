import 'package:calendar_page/calendar_page.dart';
import 'package:flutter/material.dart';

import '../repositories/classified_period_repository.dart';
import '../repositories/dtos/classified_period_dto.dart';
import '../repositories/tracked_day_repository.dart';

class DayOverviewNotifier extends ChangeNotifier {
  final TrackedDayRepository _trackedDayRepository;
  final ClassifiedPeriodRepository _classifiedPeriodRepository;

  DayOverviewNotifier(this._trackedDayRepository, this._classifiedPeriodRepository);

  List<ClassifiedPeriodDto>? classifiedPeriodDtos;

  final selectedIds = <int>[];
  void addToSelected(int id) {
    selectedIds.add(id);
    notifyListeners();
  }

  void removeFromSelected(int id) {
    selectedIds.remove(id);
    notifyListeners();
  }

  bool isDeleteMode = false;
  void setDeleteMode(bool _isDeleteMode) {
    isDeleteMode = _isDeleteMode;
    notifyListeners();
  }

  DateTime day = DateTime.now();
  void setDay(DateTime _day) {
    day = _day;
    notifyListeners();
  }

  Stream<List<ClassifiedPeriodDto>> streamClassifiedPeriodDtos() => _classifiedPeriodRepository.streamClassifiedPeriodDtos(day);

  Stream<CalendarPageDayData> streamCalendarPageDayData(DateTime dateTime) => _trackedDayRepository.streamCalendarPageData(dateTime);
}
