import 'package:calendar_page/calendar_page.dart';
import 'package:flutter/material.dart';

import '../repositories/tracked_day_repository.dart';

class CalendarPageNotifier extends ChangeNotifier {
  final TrackedDayRepository _trackedDayRepository;

  bool hasDisplayedFinale = false;
  List<CalendarPageDayData> calendarPageDayDataList = [];
  Future<bool> get isWeekCompleted => _trackedDayRepository.isWeekComplete();

  CalendarPageNotifier(this._trackedDayRepository) {
    _setupCalendarDays();
  }

  Future<bool> isInTrackedDays(DateTime date) async => _trackedDayRepository.isInTrackedDays(date);

  Future<void> _setupCalendarDays() async {
    await _loadCalendarPageDayDataList();
    if (calendarPageDayDataList.isEmpty) await _initializeDays();
  }

  Future<void> _loadCalendarPageDayDataList() async {
    calendarPageDayDataList = await _trackedDayRepository.getCalendarPageDayDataList();
    notifyListeners();
  }

  Future<void> _initializeDays() async {
    for (var day = 0; day <= 7; day++) {
      await _trackedDayRepository.insertTrackedDay(day);
      await _loadCalendarPageDayDataList();
    }
  }

  
}
