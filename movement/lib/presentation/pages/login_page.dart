import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:questionnaire/questionnaire.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../color_pallet.dart';
import '../../infrastructure/notifiers/auth_notifier.dart';
import '../../infrastructure/notifiers/generic_notifier.dart';
import '../../infrastructure/repositories/dtos/enums/log_type.dart';
import '../../infrastructure/repositories/log_repository.dart';
import '../../infrastructure/services/localization_service.dart';
import '../../providers.dart';
import '../../text_style.dart';
import '../questionnaire/questionnaire_mixin.dart';
import '../routing/routes.dart';
import '../widgets/elevated_button.dart';
import '../widgets/text_field.dart';

Future<void> _loginErrorNoInternetDialog(BuildContext context) async {
  await showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Row(children: [
          const FaIcon(FontAwesomeIcons.circleExclamation, size: 40, color: ColorPallet.orangeDark),
          const SizedBox(width: 20),
          Text(AppLocalizations.of(context).translate('loginpage_errormessage'), style: textStyleSourceSans20),
        ]),
        content: Text(AppLocalizations.of(context).translate('loginpage_errorexplanationtext'), style: textStyleSourceSans16Black),
        actions: [
          TextButton(
            child: Text(AppLocalizations.of(context).translate('loginpage_okay_warning'), style: textStyleSourceSans18),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

class LoginPage extends ConsumerStatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ConsumerState<LoginPage> with TextFieldMixin, CreateQuestionnaireMixin, OnQuestionnaireMixin {
  final imagePath = 'assets/images/qr_code_scanning.png';

  String _email = '';
  String _password = '';

  bool get _fieldsFilled {
    return _email.isNotEmpty && _password.isNotEmpty;
  }

  Future<bool> _login(WidgetRef ref, BuildContext context) async {
    final authNotifier = ref.read(authNotifierProvider.notifier);
    final value = await authNotifier.authenticate(_email, _password);
    print('authenticate called for username [$_email] and password [$_password], value [$value] !');
    return value;
  }

  Future<void> _launchURL() async {
    final url = 'tel:${AppLocalizations.of(context).translate('loginpage_telephonenumber')}';
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw Exception('Could not launch $url');
    }
  }

  void _goToIntroductionScreen() {
    Routes.RouteToPage('introductionPage', context, ref);
  }

  void _goToMenu() {
    Routes.RouteToPage('menuWidget', context, ref);
  }

  Future<void> _gotoNextScreen() async {
    final prefs = await SharedPreferences.getInstance();

    final startIntroduction = prefs.getBool('introduction') ?? false;

    if (startIntroduction) {
      _goToIntroductionScreen();
      await prefs.setBool('introduction', false);
    }
  }

  @override
  void onQuestionComplete(QuestionResultBase answer) {
    print('_LoginPageState: OnQuestionComplete [$answer]');
  }

  @override
  void onQuestionnaireComplete(Map<int, QuestionResultBase> answers) {
    print('_LoginPageState: OnQuestionnaireComplete [$answers]');
    // final authNotifier = ref.read(authNotifierProvider.notifier);
    // authNotifier.sendQuestionnaire(answers);
    _goToMenu();
  }

  @override
  void onTextChanged(String source, String text) {
    print('Text Changed by [$source] with value [$text]');

    if (source == AppLocalizations.of(context).translate('loginpage_usertext')) {
      setState(() {
        _email = text;
      });
    } else if (source == AppLocalizations.of(context).translate('loginpage_passwordtext')) {
      setState(() {
        _password = text;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    log('LoginPage::build', '', LogType.Flow);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Container(
          width: width,
          height: 1.2 * height,
          color: ColorPallet.primaryColor,
          child: Padding(
            padding: EdgeInsets.only(top: height * 0.10),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 40,
                  backgroundColor: const Color.fromRGBO(255, 255, 255, 0.15),
                  child: ClipOval(
                      child: Container(
                    height: 70,
                    width: 70,
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(8),
                      child: Image.asset(
                        'assets/images/cbs_logo.png',
                        width: 60,
                        height: 60,
                        fit: BoxFit.fill,
                      ),
                    ),
                  )),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
                  child: Text(
                    AppLocalizations.of(context).translate('loginpage_title'),
                    style: textStyleSoho36,
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 25),
                  child: Text(
                    AppLocalizations.of(context).translate('loginpage_explanationtext'),
                    style: textStyleAkko20,
                    textAlign: TextAlign.center,
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25, bottom: 20),
                    child: Align(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Consumer(
                            builder: (context, ref, child) {
                              final state = ref.watch(authNotifierProvider);
                              final notifier = ref.watch(authNotifierProvider.notifier);
                              return Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: ColorPallet.lightBlueWithOpacity,
                                      border: Border.all(
                                          color: (notifier.userNameCorrect || state is Initial || state is Loading)
                                              ? ColorPallet.lightBlueWithOpacity
                                              : const Color(0xFFDB7758),
                                          width: 2),
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 2),
                                      child: TextFieldWidget(
                                        isObscureText: false,
                                        source: AppLocalizations.of(context).translate('loginpage_usertext'),
                                        keyboardType: TextInputType.emailAddress,
                                        hintText: AppLocalizations.of(context).translate('loginpage_usertext'),
                                        icon: FaIcon(FontAwesomeIcons.userLarge, color: Colors.white.withOpacity(0.7)),
                                        delegate: this,
                                      ),
                                    ),
                                  ),
                                  buildUsernameError(context, ref, state, notifier),
                                ],
                              );
                            },
                          ),
                          Consumer(builder: (context, ref, child) {
                            final state = ref.watch(authNotifierProvider);
                            final notifier = ref.watch(authNotifierProvider.notifier);
                            return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: ColorPallet.lightBlueWithOpacity,
                                  border: Border.all(
                                      color: (!notifier.userNameCorrect || notifier.passwordCorrect || state is Initial || state is Loading)
                                          ? ColorPallet.lightBlueWithOpacity
                                          : const Color(0xFFDB7758),
                                      width: 2),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 2),
                                  child: TextFieldWidget(
                                    source: AppLocalizations.of(context).translate('loginpage_passwordtext'),
                                    keyboardType: TextInputType.name,
                                    hintText: AppLocalizations.of(context).translate('loginpage_passwordtext'),
                                    isObscureText: true,
                                    icon: FaIcon(FontAwesomeIcons.key, color: Colors.white.withOpacity(0.7)),
                                    delegate: this,
                                  ),
                                ),
                              ),
                              buildPasswordError(context, ref, state, notifier),
                            ]);
                          }),
                          Consumer(builder: (context, ref, child) {
                            final state = ref.watch(authNotifierProvider);
                            if (state is Loaded<bool> || state is Initial) {
                              return ElevatedButtonWidget(
                                  buttonText: AppLocalizations.of(context).translate('loginpage_loginbutton'),
                                  screenWidth: width,
                                  buttonColor: ColorPallet.lightGreen,
                                  onPressed: _fieldsFilled
                                      ? () async {
                                          final res = await _login(ref, context);
                                          if (res) {
                                            if (!mounted) return;
                                            await _gotoNextScreen();
                                          }
                                        }
                                      : null);
                            } else if (state is Loading) {
                              return Stack(children: [
                                Align(
                                  heightFactor: 1.3,
                                  child: Container(
                                    padding: const EdgeInsets.only(top: 10),
                                    height: 30,
                                    width: 20,
                                    child: const CircularProgressIndicator(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                Align(child: ElevatedButtonWidget(buttonText: '', screenWidth: width, buttonColor: ColorPallet.lightGreen, onPressed: null))
                              ]);
                            } else {
                              return ElevatedButtonWidget(
                                  buttonText: AppLocalizations.of(context).translate('loginpage_loginbutton'),
                                  screenWidth: width,
                                  buttonColor: ColorPallet.lightGreen,
                                  onPressed: _fieldsFilled
                                      ? () async {
                                          final res = await _login(ref, context);
                                          if (res) {
                                            if (!mounted) return;
                                            await _gotoNextScreen();
                                          }
                                        }
                                      : null);
                            }
                          }),
                          Padding(
                            padding: const EdgeInsets.only(top: 20),
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(children: <TextSpan>[
                                TextSpan(
                                  text: AppLocalizations.of(context).translate('loginpage_forgotpasswordtext'),
                                  style: textStyleAkko16,
                                  recognizer: TapGestureRecognizer()..onTap = _launchURL,
                                ),
                              ]),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: RichText(
                              textAlign: TextAlign.center,
                              text: TextSpan(
                                text: AppLocalizations.of(context).translate('loginpage_getincontacttext'),
                                style: textStyleAkko16,
                                recognizer: TapGestureRecognizer()..onTap = _launchURL,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildUsernameError(BuildContext context, WidgetRef ref, NotifierState state, AuthNotifier notifier) {
    if (notifier.passwordCorrect || state is Initial || state is Loading) {
      return const Padding(
        padding: EdgeInsets.only(top: 15),
      );
    } else if (!notifier.userNameCorrect && state is! Initial) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xFFDB7758),
          ),
          alignment: Alignment.centerLeft,
          child: const Padding(
            padding: EdgeInsets.only(top: 15, bottom: 15, left: 15),
            child: Text(
              'Gebruikersnaam ongeldig',
              style: TextStyle(color: Colors.white),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      );
    } else {
      return const Padding(
        padding: EdgeInsets.only(top: 15),
      );
    }
  }

  Widget buildPasswordError(BuildContext context, WidgetRef ref, NotifierState state, AuthNotifier notifier) {
    final diff = notifier.lastAttempt.add(const Duration(seconds: 60)).difference(notifier.thisAttempt).inSeconds;
    final attempts = 3 - notifier.attempts;
    if ((notifier.isTimeLocked || attempts < 0) && diff > 0 && notifier.userNameCorrect && state is! Initial) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xFFDB7758),
          ),
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15, left: 15),
            child: Text(
              'U moet nog $diff seconden wachten',
              style: const TextStyle(color: Colors.white),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      );
    } else if (notifier.passwordCorrect || state is Initial || state is Loading) {
      return const Padding(
        padding: EdgeInsets.only(top: 15),
      );
    } else if (!notifier.isTimeLocked && state is! Initial && notifier.userNameCorrect && attempts >= 0) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 15),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: const Color(0xFFDB7758),
          ),
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: const EdgeInsets.only(top: 15, bottom: 15, left: 15),
            child: Text(
              'Wachtwoord ongeldig. Nog ${attempts + 1} pogingen.',
              style: const TextStyle(color: Colors.white),
              textAlign: TextAlign.left,
            ),
          ),
        ),
      );
    } else if (notifier.passwordCorrect || state is Initial || state is Loading) {
      return const Padding(
        padding: EdgeInsets.only(top: 15),
      );
    } else {
      return const Padding(
        padding: EdgeInsets.only(top: 15),
      );
    }
  }
}
