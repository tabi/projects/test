import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../app_fonts.dart';
import '../../../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../../../infrastructure/services/localization_service.dart';
import '../../../routing/routes.dart';

class VehicleForm extends ConsumerWidget {
  final MovementDto movementDto;
  const VehicleForm(this.movementDto);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        InkWell(
          onTap: () => Routes.RouteToPage('movementVehiclePage', context, ref, movementDto: movementDto),
          child: Container(
            alignment: Alignment.center,
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            height: MediaQuery.of(context).size.height * 0.06,
            width: MediaQuery.of(context).size.width * 0.8,
            decoration: BoxDecoration(
              border: Border.all(color: const Color.fromRGBO(51, 66, 91, 0.15), width: 2),
              borderRadius: const BorderRadius.all(
                Radius.circular(10),
              ),
            ),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(left: 15),
                  child: const FaIcon(
                    FontAwesomeIcons.locationDot,
                    color: Color(0xFF00A1CD),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    movementDto.vehicle?.name ?? AppLocalizations.of(context).translate('movementdetailspage_nomovement'),
                    style: movementDto.vehicle?.name == null ? AppFonts.grayedOutNormalText : const TextStyle(),
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
