import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../color_pallet.dart';
import '../../../../infrastructure/repositories/dtos/stop_dto.dart';
import '../../../../providers.dart';

class DeleteButton extends ConsumerWidget {
  final StopDto stopDto;
  const DeleteButton(this.stopDto);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final stopNotifier = ref.watch(stopNotifierProvider(stopDto));
    return InkWell(
      onTap: () {
        stopNotifier.delete();
        Navigator.of(context).pop();
      },
      child: const Padding(
        padding: EdgeInsets.all(8),
        child: Icon(
          Icons.delete,
          size: 30,
          color: ColorPallet.pink,
        ),
      ),
    );
  }
}
