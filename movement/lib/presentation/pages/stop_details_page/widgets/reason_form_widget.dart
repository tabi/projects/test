import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../app_fonts.dart';
import '../../../../infrastructure/repositories/dtos/stop_dto.dart';
import '../../../../infrastructure/services/localization_service.dart';
import '../../../routing/routes.dart';
import '../../../theme/icon_mapper.dart';

class ReasonForm extends ConsumerWidget {
  final StopDto stopDto;
  const ReasonForm(this.stopDto);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return InkWell(
      onTap: () => Routes.RouteToPage('stopReasonPage', context, ref, stopDto: stopDto),
      child: Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
        height: MediaQuery.of(context).size.height * 0.06,
        width: MediaQuery.of(context).size.width * 0.8,
        decoration: BoxDecoration(
            border: Border.all(color: const Color.fromRGBO(51, 66, 91, 0.15), width: 2), borderRadius: const BorderRadius.all(Radius.circular(10))),
        child: Row(
          children: [
            if (stopDto.reason?.icon == null)
              Container(margin: const EdgeInsets.only(left: 32))
            else
              Container(margin: const EdgeInsets.only(left: 15), child: FaIconMapper.getFaIcon(stopDto.reason?.icon)),
            Container(
              margin: const EdgeInsets.only(left: 15),
              width: MediaQuery.of(context).size.width * 0.55,
              child: Text(
                stopDto.reason?.name ?? AppLocalizations.of(context).translate('locationdetailspage_noreason'),
                style: stopDto.reason?.name == null ? AppFonts.grayedOutNormalText : const TextStyle(),
              ),
            ),
            const Icon(
              Icons.arrow_forward_ios_outlined,
              size: 15,
            )
          ],
        ),
      ),
    );
  }
}
