import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../infrastructure/repositories/dtos/classified_period_dto.dart';
import '../../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../../infrastructure/repositories/dtos/stop_dto.dart';
import '../../../providers.dart';
import 'widgets/fancy_fab.dart';
import 'widgets/header_widget.dart';
import 'widgets/missing_tile.dart';
import 'widgets/movement_tile_widget.dart';
import 'widgets/movements_and_stops_map.dart';
import 'widgets/progress_bar_widget.dart';
import 'widgets/stop_tile_widget.dart';

class DayOverviewPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      floatingActionButton: const FancyFab(),
      body: Column(
        children: const [
          HeaderWidget(),
          MovementsAndStopsMap(),
          ProgressBar(),
          SizedBox(height: 10),
          ClassifiedPeriodList(),
        ],
      ),
    );
  }
}

class ClassifiedPeriodList extends ConsumerWidget {
  const ClassifiedPeriodList();

  Widget _addMissingPeriod(Widget widget, ClassifiedPeriodDto current, ClassifiedPeriodDto? next) {
    if (next == null) return widget;
    final startDate = current.classifiedPeriod.endDate;
    final endDate = next.classifiedPeriod.startDate;
    if (endDate.difference(startDate).inMinutes <= 2) return widget;
    return Column(children: [widget, MissingTile(startDate, endDate)]);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);
    return StreamBuilder<List<ClassifiedPeriodDto>>(
      stream: dayOverviewNotifier.streamClassifiedPeriodDtos(),
      builder: (_, AsyncSnapshot<List<ClassifiedPeriodDto>> snapshot) {
        if (snapshot.hasData == false || snapshot.data!.isEmpty) {
          return const SizedBox(height: 300, child: Center(child: Text('Laden..')));
        }
        final dtos = snapshot.data!;
        return Expanded(
          child: ListView.builder(
            itemCount: dtos.length,
            itemBuilder: (context, index) {
              final dto = dtos[index];
              final next = dtos.length > index + 1 ? dtos[index + 1] : null;
              if (dto is StopDto) return _addMissingPeriod(StopTile(dto, index == dtos.length - 1), dto, next);
              if (dto is MovementDto) return _addMissingPeriod(MovementTile(dto, index == dtos.length - 1), dto, next);
              return const SizedBox();
            },
          ),
        );
      },
    );
  }
}
