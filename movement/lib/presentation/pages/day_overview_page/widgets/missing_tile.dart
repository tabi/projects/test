import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';

import '../../../../color_pallet.dart';
import '../../../routing/routes.dart';
import '../../../widgets/diagonal_striped_widget.dart';

class MissingTile extends ConsumerWidget {
  final DateTime startDate;
  final DateTime endDate;

  const MissingTile(this.startDate, this.endDate);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Stack(
      children: [
        Positioned(
          top: 5,
          child: Container(
            height: 80,
            width: MediaQuery.of(context).size.width,
            child: DiagonalStipedWidget(Color(0xFFEBEEF0).withOpacity(0.3), Colors.white, 10, 100),
          ),
        ),
        Container(
          height: 90,
          padding: const EdgeInsets.only(left: 10),
          child: Row(
            children: [
              Container(
                width: 4,
                color: ColorPallet.lightGrayishBlue,
                margin: const EdgeInsets.only(right: 10, left: 2),
              ),
              SizedBox(width: 15),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Missende data', style: Theme.of(context).primaryTextTheme.headline1),
                  Text('${DateFormat('Hm').format(startDate)} - ${DateFormat('Hm').format(endDate)}', style: Theme.of(context).primaryTextTheme.bodyText1),
                ],
              ),
              Expanded(child: SizedBox()),
              AddPeriodPopMenu(startDate, endDate),
              SizedBox(width: 20),
            ],
          ),
        ),
      ],
    );
  }
}

class AddPeriodPopMenu extends ConsumerWidget {
  final DateTime startDate;
  final DateTime endDate;

  const AddPeriodPopMenu(this.startDate, this.endDate);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: PopupMenuButton<String>(
        color: Colors.white,
        itemBuilder: (BuildContext context) => <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
            value: 'locatie',
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
                Routes.RouteToPage('stopDetailsPage', context, ref, startDate: startDate, endDate: endDate);
              },
              child: Row(
                children: <Widget>[
                  SizedBox(width: 5),
                  FaIcon(FontAwesomeIcons.locationDot, color: ColorPallet.darkTextColor),
                  SizedBox(width: 17),
                  Text('Locatie toevoegen', style: TextStyle(color: ColorPallet.darkTextColor)),
                ],
              ),
            ),
          ),
          PopupMenuItem<String>(
            value: 'verplaatsing',
            child: InkWell(
              onTap: () {
                Navigator.of(context).pop();
                Routes.RouteToPage('movementDetailsPage', context, ref, startDate: startDate, endDate: endDate);
              },
              child: Row(
                children: <Widget>[
                  SizedBox(width: 5),
                  FaIcon(FontAwesomeIcons.route, color: ColorPallet.darkTextColor),
                  SizedBox(width: 17),
                  Text('Verplaatsing toevoegen', style: TextStyle(color: ColorPallet.darkTextColor)),
                ],
              ),
            ),
          ),
        ],
        child: const Icon(Icons.more_vert, color: ColorPallet.darkTextColor),
      ),
    );
  }
}
