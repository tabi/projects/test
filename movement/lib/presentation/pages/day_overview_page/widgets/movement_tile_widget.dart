import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../color_pallet.dart';
import '../../../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../../../infrastructure/services/localization_service.dart';
import '../../../../providers.dart';
import '../../../routing/routes.dart';
import '../../../theme/icon_mapper.dart';
import '../get_date_string.dart';

class MovementTile extends ConsumerWidget {
  final MovementDto movementDto;
  final bool isLastIndex;
  const MovementTile(this.movementDto, this.isLastIndex);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);
    final classifiedPeriod = movementDto.classifiedPeriod;
    final isTapped = dayOverviewNotifier.selectedIds.contains(classifiedPeriod.uuid);

    return InkWell(
      child: Container(
        height: 90,
        padding: const EdgeInsets.only(left: 10),
        color: isTapped ? Colors.black12 : Colors.transparent,
        child: Row(
          children: [
            Container(
              width: 4,
              color: ColorPallet.lightGrayishBlue,
              margin: const EdgeInsets.only(right: 10, left: 2),
            ),
            Container(
              margin: const EdgeInsets.only(right: 10),
              color: Colors.white,
              child: Icon(
                Icons.check_circle,
                size: 25,
                color: movementDto.classifiedPeriod.confirmed ? ColorPallet.lightGreen : ColorPallet.veryDarkGray,
              ),
            ),
            FaIconMapper.getFaIcon(movementDto.vehicle?.icon),
            Container(
              width: MediaQuery.of(context).size.width * 0.55,
              margin: const EdgeInsets.only(
                left: 10,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(movementDto.vehicle?.name == null ? AppLocalizations.of(context).translate('movementpage_unknown') : movementDto.vehicle!.name!,
                      style: Theme.of(context).primaryTextTheme.headline1),
                  Text(getDateString(classifiedPeriod, isLastIndex), style: Theme.of(context).primaryTextTheme.bodyText1)
                ],
              ),
            ),
            const Expanded(child: SizedBox()),
            if (movementDto.classifiedPeriod.confirmed)
              Container()
            else
              Container(
                height: 10,
                width: 10,
                alignment: Alignment.centerRight,
                decoration: BoxDecoration(
                  color: movementDto.classifiedPeriod.confirmed ? ColorPallet.lightGreen : ColorPallet.orange,
                  borderRadius: const BorderRadius.all(
                    Radius.circular(10),
                  ),
                ),
              ),
            const SizedBox(width: 35),
          ],
        ),
      ),
      onTap: () {
        if (dayOverviewNotifier.isDeleteMode) {
          // TODO: re-implement removed code related to removing stops
        } else {
          Routes.RouteToPage('movementDetailsPage', context, ref, movementDto: movementDto);
        }
      },
      onLongPress: () {
        dayOverviewNotifier.isDeleteMode = true;
        // TODO: re-implement removed code related to removing stops
      },
    );
  }
}
