import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../color_pallet.dart';
import '../../../../infrastructure/repositories/dtos/stop_dto.dart';
import '../../../../infrastructure/services/localization_service.dart';
import '../../../../providers.dart';
import '../../../routing/routes.dart';
import '../../../theme/icon_mapper.dart';
import '../get_date_string.dart';

class StopTile extends ConsumerWidget {
  final StopDto stopDto;
  final bool isLastIndex;
  const StopTile(this.stopDto, this.isLastIndex);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);
    final classifiedPeriod = stopDto.classifiedPeriod;
    final isTapped = dayOverviewNotifier.selectedIds.contains(classifiedPeriod.uuid);

    return Container(
      height: 90,
      padding: const EdgeInsets.only(left: 10),
      child: Row(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: 4,
                color: ColorPallet.lightGrayishBlue,
                margin: const EdgeInsets.only(right: 10, left: 2),
              ),
              Positioned(
                left: 1,
                top: 40,
                child: Container(
                  height: 6,
                  width: 6,
                  decoration: BoxDecoration(
                    color: stopDto.classifiedPeriod.confirmed ? ColorPallet.lightGreen : ColorPallet.orange,
                    borderRadius: const BorderRadius.all(Radius.circular(10)),
                  ),
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                color: isTapped ? Colors.black12 : Colors.transparent,
                child: InkWell(
                  child: Row(
                    children: [
                      Stack(children: <Widget>[
                        Container(
                          height: 80,
                          width: 80,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(bottom: 10, right: 10, left: 5),
                          decoration: BoxDecoration(
                            border: Border.all(color: ColorPallet.veryDarkGray, width: 2),
                            borderRadius: const BorderRadius.all(
                              Radius.circular(10),
                            ),
                          ),
                          child: FaIconMapper.getFaIcon(stopDto.reason?.icon),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            margin: EdgeInsets.zero,
                            padding: EdgeInsets.zero,
                            color: Colors.white,
                            child: Icon(
                              Icons.check_circle,
                              size: 30,
                              color: stopDto.classifiedPeriod.confirmed ? ColorPallet.lightGreen : ColorPallet.veryDarkGray,
                            ),
                          ),
                        ),
                      ]),
                      Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          margin: const EdgeInsets.only(left: 5, bottom: 15),
                          child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                            Text(
                                stopDto.googleMapsData?.name == null
                                    ? AppLocalizations.of(context).translate('movementpage_nolocation')
                                    : stopDto.googleMapsData!.name!.split(',')[0],
                                style: Theme.of(context).primaryTextTheme.headline1),
                            Text(getDateString(classifiedPeriod, isLastIndex), style: Theme.of(context).primaryTextTheme.bodyText1)
                          ])),
                      if (stopDto.classifiedPeriod.confirmed)
                        Container()
                      else
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Container(
                            height: 10,
                            width: 10,
                            decoration: BoxDecoration(
                              color: stopDto.classifiedPeriod.confirmed ? ColorPallet.lightGreen : ColorPallet.orange,
                              borderRadius: const BorderRadius.all(
                                Radius.circular(10),
                              ),
                            ),
                          ),
                        ),
                      const SizedBox(width: 35),
                    ],
                  ),
                  onTap: () {
                    if (dayOverviewNotifier.isDeleteMode) {
                      // TODO: re-implement removed code related to removing stops
                    } else {
                      Routes.RouteToPage('stopDetailsPage', context, ref, stopDto: stopDto);
                    }
                  },
                  onLongPress: () {
                    dayOverviewNotifier.isDeleteMode = true;
                    // TODO: re-implement removed code related to removing stops
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
