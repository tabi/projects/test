import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../../../color_pallet.dart';
import '../../../../providers.dart';
import '../../../locale/dutch_date_locale.dart';

class HeaderWidget extends ConsumerWidget {
  const HeaderWidget();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);
    return Container(
      height: 70,
      width: MediaQuery.of(context).size.width,
      color: ColorPallet.primaryColor,
      child: Stack(
        children: [
          Center(
            child: Text(
              formatDate(
                dayOverviewNotifier.day,
                [MM, ' ', dd],
                locale: const DutchDateLocale(),
              ),
              style: const TextStyle(
                color: Colors.white,
                fontSize: 22,
              ),
            ),
          ),
          Positioned(
            right: 20,
            top: 20,
            child: Icon(
              Icons.check,
              color: Colors.white.withOpacity(0.5),
              size: 30,
            ),
          )
        ],
      ),
    );
  }
}