import 'dart:async';

import 'package:calendar_page/calendar_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../color_pallet.dart';
import '../../infrastructure/repositories/dtos/enums/log_type.dart';
import '../../infrastructure/repositories/log_repository.dart';
import '../../infrastructure/services/localization_service.dart';
import '../../providers.dart';
import '../../text_style.dart';
import '../animations/confetti_animation.dart';

class CalendarPageAdapter extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final menuNotifier = ref.watch(menuNotifierProvider);
    final calendarPageNotifier = ref.watch(calendarPageNotifierProvider);
    // final dayNotifier = ref.watch(dayNotifierProvider.notifier);
    final dayOverviewNotifier = ref.watch(dayOverviewNotifierProvider);
    if (calendarPageNotifier.calendarPageDayDataList.isEmpty) return const SizedBox();
    return CalendarPage(
        calendarData: calendarPageNotifier.calendarPageDayDataList,
        onDatePressed: (DateTime dateTime) async {
          if (await calendarPageNotifier.isInTrackedDays(dateTime)) {
            unawaited(log('MenuWidget::onDatePressed', dateTime.toString(), LogType.Flow));
            // dayNotifier.setDay(dateTime);
            dayOverviewNotifier.day = dateTime;
            unawaited(menuNotifier.setPage(1));
          } else {
            unawaited(Fluttertoast.showToast(
              msg: AppLocalizations.of(context).translate('calendarpage_toastwarning'),
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
            ));
          }
        });
  }
}

Future<void> weekCompletedDialog(BuildContext context, WidgetRef ref) async {
  await log('weekCompletedDialog::build', '', LogType.Flow);
  await showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return StatefulBuilder(builder: (context, setState) {
        return Stack(
          children: [
            Dialog(
              insetPadding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5)),
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 20),
                        child: Center(
                            child: SvgPicture.asset(
                          'assets/images/congratz.svg',
                        )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: Row(
                          children: [
                            Expanded(
                                flex: 6,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 30),
                                      child: Text(
                                        AppLocalizations.of(context).translate('completeweek_dialogue_title'),
                                        style: textStyleSoho24Black,
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                )),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: Text(
                          AppLocalizations.of(context).translate('completeweek_dialogue_text'),
                          style: textStyleAkko16Grey,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.07),
                            primary: ColorPallet.lightBlueWithOpacity,
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                          onPressed: () {},
                          child: Text(
                            AppLocalizations.of(context).translate('completeweek_dialogue_overview'),
                            style: textStyleAkko16White,
                          ),
                        ),
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.07),
                            primary: Colors.white,
                            elevation: 4,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5), side: const BorderSide(color: ColorPallet.lightBlueWithOpacity, width: 4)),
                          ),
                          onPressed: () {},
                          child: Text(
                            AppLocalizations.of(context).translate('completeweek_dialogue_logout'),
                            style: textStyleAkko16Blue,
                          ),
                        ),
                      ),
                      TextButton(
                        child: Text(AppLocalizations.of(context).translate('completeweek_dialogue_cancel'), style: textStyleAkko16Blue),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const ConfettiAnimation(),
          ],
        );
      });
    },
  );
}
