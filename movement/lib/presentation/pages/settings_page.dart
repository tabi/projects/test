import 'package:drift_db_viewer/drift_db_viewer.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../color_pallet.dart';
import '../../infrastructure/notifiers/generic_notifier.dart';
import '../../infrastructure/repositories/dtos/enums/log_type.dart';
import '../../infrastructure/repositories/dtos/setting_dto.dart';
import '../../infrastructure/repositories/log_repository.dart';
import '../../infrastructure/services/localization_service.dart';
import '../../providers.dart';
import '../../text_style.dart';
import '../widgets/elevated_button.dart';

class SettingsPage extends ConsumerWidget {
  final emailText = 'mobiledevelopment@cbs.nl';
  final telephoneNumber = '0455 70 65 34';
  final faqText = 'www.cbs.nl/apptest';

  Future<void> _launchEmailURL() async {
    await log('SettingsPage::_launchEmailURL', 'url: $emailText', LogType.Flow);
    final emailLaunchUri = Uri(
      scheme: 'mailto',
      path: emailText,
    );

    final email = emailLaunchUri.toString();
    if (await canLaunchUrl(Uri.parse(email))) {
      await launchUrl(Uri.parse(email));
    } else {
      throw Exception('Could not launch $email');
    }
  }

  Future<void> _launchPhoneURL() async {
    await log('SettingsPage::_launchPhoneURL', 'telephoneNumber: $telephoneNumber', LogType.Flow);
    final url = 'tel:$telephoneNumber';
    if (await canLaunchUrl(Uri.parse(url))) {
      await launchUrl(Uri.parse(url));
    } else {
      throw Exception('Could not launch $url');
    }
  }

  Future<void> _launchFaqURL() async {
    await log('SettingsPage::_launchFaqURL', 'url: $faqText', LogType.Flow);
    final faqLaunchUri = Uri(
      path: faqText,
    );

    final faq = faqLaunchUri.toString();
    if (await canLaunchUrl(Uri.parse(faq))) {
      await launchUrl(Uri.parse(faq));
    } else {
      throw Exception('Could not launch $faq');
    }
  }

  String _formatTimeOfDay(TimeOfDay tod) {
    final now = DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    final format = DateFormat('HH:mm');
    return format.format(dt);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    log('SettingsPage::build', '', LogType.Flow);
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Theme.of(context).primaryColor,
          title: const Text('Instellingen'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            getContactInformationWidget(context, ref),
            const SizedBox(
              height: 2,
            ),
            getLocationReactivationWidget(context, ref),
            const SizedBox(
              height: 2,
            ),
            getLocationCountWidget(context, ref),
            const SizedBox(
              height: 2,
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push<dynamic>(MaterialPageRoute<dynamic>(
                  builder: (context) => DriftDbViewer(
                    ref.read(database),
                  ),
                ));
              },
              child: const Text('Show database'),
            ),
            ElevatedButton(
              onPressed: () {
                ref.read(classifiedPeriodRepository).syncClassifiedPeriods();
              },
              child: const Text('sync classified period'),
            ),
            ElevatedButton(
              onPressed: () async {
                // final authHeader =
                //     'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VySUQiOjExLCJleHAiOjE2NTMyNTE5MzksImlzcyI6IkF1dGhTZXJ2aWNlIn0.vVxdHuyGOEg9ew4hPzNg6GZ5xPrpNXpeaKKsjUuFONo';
                // const serverSocketAddress = 'http://192.168.1.7:8000';
                // final uri = Uri.parse('$serverSocketAddress/ping');
                // final uri = Uri.parse('$serverSocketAddress/api/sensor_geolocation');
                // http.Response response = await http.get(uri, headers: {'Content-Type': 'application/json', HttpHeaders.authorizationHeader: authHeader});
                // final response = await http.get(uri).timeout(Duration(seconds: 2));
                await ref.read(sensorRepository).syncSensorGeolocations();
                // await ref.read(deviceRepository).checkIfDeviceIsStored();
                // print(response);
                // print(11242342);
              },
              child: const Text('Test sync'),
            ),
            ElevatedButton(
              onPressed: () async {
                await ref.read(foregroundServiceProvider).startForegroundServiceAsync();
              },
              child: const Text('startForegroundServiceAsync'),
            ),
          ]),
        ));
  }

  Widget getLocationCountWidget(BuildContext context, WidgetRef ref) {
    return Row(children: [
      Expanded(
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Consumer(builder: (context, ref, child) {
                  final state = ref.watch(settingsNotifierProvider);
                  if (state is Loaded<SettingsDTO>) {
                    return Text('LocationCount: ${state.loadedObject.locationCount}', style: textStyleAkko16Grey);
                  } else {
                    return const LinearProgressIndicator();
                  }
                }),
              ),
              const SizedBox(
                height: 2,
              ),
              ElevatedButtonWidget(
                buttonText: 'Refresh',
                screenWidth: MediaQuery.of(context).size.width,
                buttonColor: ColorPallet.primaryColor,
                onPressed: () async {
                  final notifier = ref.watch(settingsNotifierProvider.notifier);
                  notifier.refreshLocationCount();
                },
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 12),
              )
            ]),
          ),
        ),
      ),
    ]);
  }

  Widget getLocationReactivationWidget(BuildContext context, WidgetRef ref) {
    return Row(children: [
      Expanded(
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(AppLocalizations.of(context).translate('introductionpage_locationsharing'), style: textStyleSoho20Black),
              ),
              const SizedBox(
                height: 2,
              ),
              ElevatedButtonWidget(
                buttonText: 'Instellingen',
                screenWidth: MediaQuery.of(context).size.width,
                buttonColor: ColorPallet.primaryColor,
                onPressed: () async {
                  await Geolocator.openAppSettings();
                },
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 12),
              )
            ]),
          ),
        ),
      ),
    ]);
  }

  Widget getLogoutWidget(BuildContext context, WidgetRef ref) {
    return Row(
      children: [
        Expanded(
          child: Card(
            margin: EdgeInsets.zero,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 20),
              child: Column(
                children: [
                  ElevatedButton(
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xFF26AFD5))),
                    onPressed: () {
                      Navigator.popUntil(context, ModalRoute.withName('/'));
                      ref.read(settingsNotifierProvider.notifier).logout();
                    },
                    child: Text(AppLocalizations.of(context).translate('settingspage_logoff')),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget getContactInformationWidget(BuildContext context, WidgetRef ref) {
    return Row(children: [
      Expanded(
        child: Card(
          margin: EdgeInsets.zero,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
            child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Text(AppLocalizations.of(context).translate('settingspage_contact'), style: textStyleSoho20Black),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
                child: ListTile(
                  title: Text(
                    AppLocalizations.of(context).translate('settingspage_phonenumber'),
                    style: textStyleAkko16Black,
                  ),
                  contentPadding: const EdgeInsets.only(right: 10, bottom: 10),
                  dense: true,
                  trailing: InkWell(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: AppLocalizations.of(context).translate('settingspage_telephonenumber'),
                          style: textStyleAkko16Blue,
                          recognizer: TapGestureRecognizer()..onTap = _launchPhoneURL,
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
                child: ListTile(
                  title: Text(
                    AppLocalizations.of(context).translate('settingspage_email'),
                    style: textStyleAkko16Black,
                  ),
                  contentPadding: const EdgeInsets.only(right: 10, bottom: 10),
                  dense: true,
                  trailing: InkWell(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: AppLocalizations.of(context).translate('settingspage_urltext'),
                          style: textStyleAkko16Blue,
                          recognizer: TapGestureRecognizer()..onTap = _launchPhoneURL,
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
                child: ListTile(
                  title: Text(
                    AppLocalizations.of(context).translate('settingspage_faq'),
                    style: textStyleAkko16Black,
                  ),
                  contentPadding: const EdgeInsets.only(right: 10, bottom: 10),
                  dense: true,
                  trailing: InkWell(
                    child: RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: <TextSpan>[
                        TextSpan(
                          text: AppLocalizations.of(context).translate('settingspage_faqurl'),
                          style: textStyleAkko16Blue,
                          recognizer: TapGestureRecognizer()..onTap = _launchFaqURL,
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
              const Padding(
                padding: EdgeInsets.only(bottom: 12),
              )
            ]),
          ),
        ),
      ),
    ]);
  }

  Widget getAccessibilityWidget(BuildContext context, WidgetRef ref) {
    return Row(
      children: [
        Expanded(
          child: Card(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
              child: Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(bottom: 5),
                      child: Text('Toegankelijkheid', style: textStyleSoho20Black),
                    ),
                    Consumer(builder: (context, ref, child) {
                      final state = ref.watch(settingsNotifierProvider);
                      if (state is Loaded<SettingsDTO>) {
                        return SizedBox(
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: SwitchListTile(
                            title: const Text(
                              'Toegankelijkheids modus',
                              style: textStyleAkko16Black,
                            ),
                            contentPadding: EdgeInsets.zero,
                            dense: true,
                            activeColor: ColorPallet.primaryColor,
                            value: state.loadedObject.accesibilityMode!,
                            onChanged: (val) {
                              log('SettingsPage::accesibilityMode', 'value: $val', LogType.Flow);
                              ref.read(settingsNotifierProvider.notifier).setAccesibilityMode(val);
                            },
                          ),
                        );
                      } else {
                        return const CircularProgressIndicator();
                      }
                    })
                  ],
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget getAlgoritmeWidget(BuildContext context, WidgetRef ref) {
    return Row(
      children: [
        Expanded(
          child: Card(
            margin: EdgeInsets.zero,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(AppLocalizations.of(context).translate('settingspage_algorithm'), style: textStyleSoho20Black),
                  ),
                  Consumer(builder: (context, ref, child) {
                    final state = ref.watch(settingsNotifierProvider);
                    if (state is Loaded<SettingsDTO>) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.06,
                        child: SwitchListTile(
                          title: Text(
                            AppLocalizations.of(context).translate('settingspage_generatedata'),
                            style: textStyleAkko16Black,
                          ),
                          contentPadding: EdgeInsets.zero,
                          dense: true,
                          activeColor: ColorPallet.primaryColor,
                          value: state.loadedObject.notificationReminder!,
                          onChanged: (val) {
                            log('SettingsPage::notificationReminder', 'value: $val', LogType.Flow);
                            ref.read(settingsNotifierProvider.notifier).setNotificationReminder(val);
                          },
                        ),
                      );
                    } else {
                      return const CircularProgressIndicator();
                    }
                  }),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget getSyncDataWidget(BuildContext context, WidgetRef ref) {
    return Row(
      children: [
        Expanded(
          child: Card(
            margin: EdgeInsets.zero,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 15),
                    child: Text(AppLocalizations.of(context).translate('settingspage_syncdata'), style: textStyleSoho20Black),
                  ),
                  ElevatedButton(
                    style: ButtonStyle(backgroundColor: MaterialStateProperty.all(const Color(0xFF26AFD5))),
                    onPressed: () {
                      ref.read(settingsNotifierProvider.notifier).uploadData();
                    },
                    child: Text(AppLocalizations.of(context).translate('settingspage_uploaddata')),
                  )
                ],
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget getNotificationWidget(BuildContext context, WidgetRef ref) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Card(
            margin: EdgeInsets.zero,
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 5),
                    child: Text(AppLocalizations.of(context).translate('settingspage_notifications'), style: textStyleSoho20Black),
                  ),
                  Consumer(builder: (context, watch, child) {
                    final state = ref.watch(settingsNotifierProvider);
                    if (state is Loaded<SettingsDTO>) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height * 0.06,
                        child: SwitchListTile(
                          title: Text(
                            AppLocalizations.of(context).translate('settingspage_dailyreminder'),
                            style: textStyleAkko16Black,
                          ),
                          contentPadding: EdgeInsets.zero,
                          dense: true,
                          activeColor: ColorPallet.primaryColor,
                          value: state.loadedObject.notificationReminder!,
                          onChanged: (val) {
                            log('SettingsPage::notificationReminder', 'value: $val', LogType.Flow);
                            ref.read(settingsNotifierProvider.notifier).setNotificationReminder(val);
                          },
                        ),
                      );
                    } else {
                      return const CircularProgressIndicator();
                    }
                  }),
                  Consumer(builder: (context, watch, child) {
                    final state = ref.watch(settingsNotifierProvider);
                    if (state is Loaded<SettingsDTO>) {
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: SizedBox(
                          height: MediaQuery.of(context).size.height * 0.06,
                          child: ListTile(
                              title: Text(
                                AppLocalizations.of(context).translate('settingspage_time'),
                                style: textStyleAkko16Black,
                              ),
                              contentPadding: const EdgeInsets.only(right: 10, bottom: 10),
                              dense: true,
                              trailing: InkWell(child: Text(_formatTimeOfDay(state.loadedObject.notificationTime!), style: textStyleAkko18Blue)),
                              onTap: () async {
                                final result = await showTimePicker(context: context, initialTime: state.loadedObject.notificationTime!);

                                if (result != null) {
                                  await log('SettingsPage::notificationTime', 'value: $result', LogType.Flow);
                                  ref.read(settingsNotifierProvider.notifier).setNotificationTime(TimeOfDay(hour: result.hour, minute: result.minute));
                                }
                              }),
                        ),
                      );
                    } else {
                      return const CircularProgressIndicator();
                    }
                  })
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
