import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../infrastructure/services/localization_service.dart';
import '../../../text_style.dart';

class QuestionsIntroductionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 25, top: 20, right: 25, bottom: 20),
        child: Column(
          children: [
            Row(
              children: [Text(AppLocalizations.of(context).translate('questionsintroductionpage_title'), style: textStyleSoho24Black)],
            ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 50),
                child: Column(
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.width * 0.7,
                      child: SvgPicture.asset('assets/images/4.svg'),
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.05,
                    ),
                    Text(
                      AppLocalizations.of(context).translate('questionsintroductionpage_questions'),
                      style: textStyleSoho20Black,
                      textAlign: TextAlign.center,
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: AppLocalizations.of(context).translate('questionsintroductionpage_helptext1'),
                            style: textStyleAkko16Black,
                          ),
                          const WidgetSpan(
                            child: Padding(
                              padding: EdgeInsets.all(3),
                              child: Icon(Icons.settings, size: 20),
                            ),
                            alignment: PlaceholderAlignment.middle,
                          ),
                          TextSpan(
                            text: AppLocalizations.of(context).translate('questionsintroductionpage_helptext2'),
                            style: textStyleAkko16Black,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }
}
