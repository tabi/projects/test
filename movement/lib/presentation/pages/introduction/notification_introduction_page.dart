import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../color_pallet.dart';
import '../../../infrastructure/services/localization_service.dart';
import '../../../providers.dart';
import '../../../text_style.dart';
import '../../widgets/elevated_button.dart';

class NotificationIntroductionPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
        padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
        child: Column(
          children: [
            Row(
              children: [Text(AppLocalizations.of(context).translate('notificationintroduction_title'), style: textStyleSoho24Black)],
            ),
            Expanded(
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.2,
                    width: MediaQuery.of(context).size.width * 0.7,
                    child: SvgPicture.asset('assets/images/3.svg'),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.05,
                  ),
                  Text(
                    AppLocalizations.of(context).translate('notificationintroduction_notifytitle'),
                    style: textStyleSoho20Black,
                    textAlign: TextAlign.center,
                  ),
                  const SizedBox(
                    height: 28,
                  ),
                  SizedBox(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Text(
                            AppLocalizations.of(context).translate('notificationintroduction_notifytext'),
                            style: textStyleAkko16Black,
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.height * 0.02,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: FractionalOffset.bottomCenter,
                      child: Consumer(
                        builder: (context, ref, child) {
                          final notifier = ref.watch(introNotificationNotifierProvider);
                          if (!notifier.isPressed) {
                            return ElevatedButtonWidget(
                              buttonText: AppLocalizations.of(context).translate('introductionpage_notificationsbutton'),
                              screenWidth: MediaQuery.of(context).size.width,
                              buttonColor: ColorPallet.primaryColor,
                              onPressed: notifier.configureLocalNotifcations,
                            );
                          } else {
                            if (notifier.isGranted) {
                              return ElevatedButtonWidget(
                                buttonText: AppLocalizations.of(context).translate('introductionpage_notificationsbuttonsuccess'),
                                screenWidth: MediaQuery.of(context).size.width,
                                buttonColor: ColorPallet.lightGreen,
                                onPressed: () {},
                              );
                            } else {
                              return ElevatedButtonWidget(
                                buttonText: AppLocalizations.of(context).translate('introductionpage_notificationsbuttonfailed'),
                                screenWidth: MediaQuery.of(context).size.width,
                                buttonColor: ColorPallet.midGray,
                                onPressed: () {},
                              );
                            }
                          }
                        },
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
