import 'dart:io';

import 'package:background_locator/background_locator.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:questionnaire/questionnaire.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../color_pallet.dart';
import '../../../infrastructure/services/ios_service.dart';
import '../../../infrastructure/services/localization_service.dart';
import '../../../providers.dart';
import '../../../text_style.dart';
import '../../questionnaire/questionnaire_mixin.dart';
import '../../routing/routes.dart';
import 'notification_introduction_page.dart';
import 'questions_introduction_page.dart';
import 'welcome_introduction_page.dart';

class DotIndicator {
  int? index;
  bool? value;
  Widget? widget;

  DotIndicator({this.index, this.value, this.widget});
}

class IntroductionPage extends ConsumerStatefulWidget {
  @override
  _IntroductionPageState createState() => _IntroductionPageState();
}

class _IntroductionPageState extends ConsumerState<IntroductionPage> with CreateQuestionnaireMixin, OnQuestionnaireMixin {
  final List<Widget> _pages = <Widget>[
    WelcomeIntroductionPage(),
    NotificationIntroductionPage(),
    QuestionsIntroductionPage(),
  ];

  final _pageController = PageController();

  double? currentPage = 0;
  int selectedIndex = 0;

  // ignore: unused_element
  Widget _dotIndicator(int index, bool isActive) {
    return Listener(
      onPointerDown: (event) {
        setState(() {
          _pageController.jumpToPage(index);
          selectedIndex = index;
        });
      },
      child: SizedBox(
        height: 15,
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 150),
          margin: const EdgeInsets.symmetric(horizontal: 4),
          height: isActive ? 15 : 10,
          width: isActive ? 15 : 10,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: ColorPallet.primaryColor),
            color: isActive ? ColorPallet.primaryColor : Colors.white,
          ),
        ),
      ),
    );
  }

  Future _requestNotificationPermission() async {
    final notifier = ref.watch(introNotificationNotifierProvider);
    await notifier.configureLocalNotifcations();
  }

  Future _requestLocationPermission() async {
    final notifier = ref.watch(introLocationNotifierProvider);
    await notifier.askLocationPermission();

    if (Platform.isIOS) {
      final data = <String, int>{'countInit': 1};

      await BackgroundLocator.registerLocationUpdate(
        CallbackHandler.iosCallback,
        initDataCallback: data,
        initCallback: CallbackHandler.init,
        iosSettings: const IOSSettings(showsBackgroundLocationIndicator: true),
      );

      await BackgroundLocator.isServiceRunning();
    } else if (Platform.isAndroid) {
      await ref.read(foregroundServiceProvider).startForegroundServiceAsync();
    }

    await notifier.checkPermission();
  }

  // void _goToQuestionnaire() {
  //   Routes.RouteToPage('questionnairePage', context, ref);
  // }

  Future<void> _goToQuestionnaireScreen() async {
    await Navigator.push<void>(
      context,
      MaterialPageRoute(
        builder: (context) => StartQuestionnairePage(
          questionList: questionList,
          colorMap: colorList,
          textMap: textList,
          delegate: this,
          storedAnswers: const <int, QuestionResultBase>{},
        ),
      ),
    );
  }

  Future<void> _goToMenu() async {
    final prefs = await SharedPreferences.getInstance();
    final startQuestionnaire = prefs.getBool('questionnaire') ?? false;

    if (!startQuestionnaire) {
      createQuestionnaire();
      await prefs.setBool('questionnaire', true);
      await _goToQuestionnaireScreen();
    } else {
      Routes.RouteToPage('menuWidget', context, ref);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 13, top: 37, right: 13, bottom: 64),
            child: PageView.builder(
              physics: const NeverScrollableScrollPhysics(),
              pageSnapping: false,
              controller: _pageController,
              itemCount: _pages.length,
              onPageChanged: (int page) {
                setState(() {
                  selectedIndex = page;
                });
              },
              itemBuilder: (BuildContext context, int index) {
                return _pages[index];
              },
            ),
          ),
          Consumer(
            builder: (context, ref, child) {
              final locationNotifier = ref.watch(introLocationNotifierProvider);
              return Positioned(
                bottom: 10,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 25, right: 25),
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              if (selectedIndex == 0)
                                TextButton(
                                  onPressed: () {
                                    _requestLocationPermission();
                                    _requestNotificationPermission();
                                    _goToMenu();
                                  },
                                  child: Text(AppLocalizations.of(context).translate('introductionpage_skip'), style: textStyleAkko16Blue),
                                )
                              else if (selectedIndex == 1)
                                TextButton(
                                  onPressed: () {
                                    _requestNotificationPermission();
                                    _goToMenu();
                                  },
                                  child: Text(AppLocalizations.of(context).translate('introductionpage_skip'), style: textStyleAkko16Blue),
                                )
                              else
                                TextButton(
                                  onPressed: _goToMenu,
                                  child: Text(AppLocalizations.of(context).translate('introductionpage_skip'), style: textStyleAkko16Blue),
                                )
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                                for (var idx = 0; idx < _pages.length; idx++)
                                  Listener(
                                    child: Consumer(builder: (context, ref, child) {
                                      final locationNotifier = ref.watch(introLocationNotifierProvider);

                                      if (!locationNotifier.isPressed && selectedIndex == 0) {
                                        return SizedBox(
                                          height: 15,
                                          child: AnimatedContainer(
                                            duration: const Duration(milliseconds: 150),
                                            margin: const EdgeInsets.symmetric(horizontal: 4),
                                            height: idx == selectedIndex ? 15 : 10,
                                            width: idx == selectedIndex ? 15 : 10,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(color: ColorPallet.midGray),
                                              color: idx == selectedIndex ? ColorPallet.midGray : Colors.white,
                                            ),
                                          ),
                                        );
                                        // } else if (!notificationNotifier.isPressed && selectedIndex == 1) {
                                        //   return SizedBox(
                                        //     height: 15,
                                        //     child: AnimatedContainer(
                                        //       duration: const Duration(milliseconds: 150),
                                        //       margin: const EdgeInsets.symmetric(horizontal: 4),
                                        //       height: idx == selectedIndex ? 15 : 10,
                                        //       width: idx == selectedIndex ? 15 : 10,
                                        //       decoration: BoxDecoration(
                                        //         shape: BoxShape.circle,
                                        //         border: Border.all(color: ColorPallet.midGray),
                                        //         color: idx == selectedIndex ? ColorPallet.midGray : Colors.white,
                                        //       ),
                                        //     ),
                                        //   );
                                      } else {
                                        return SizedBox(
                                          height: 15,
                                          child: AnimatedContainer(
                                            duration: const Duration(milliseconds: 150),
                                            margin: const EdgeInsets.symmetric(horizontal: 4),
                                            height: idx == selectedIndex ? 15 : 10,
                                            width: idx == selectedIndex ? 15 : 10,
                                            decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              border: Border.all(color: ColorPallet.primaryColor),
                                              color: idx == selectedIndex ? ColorPallet.primaryColor : Colors.white,
                                            ),
                                          ),
                                        );
                                      }
                                    }),
                                  )
                              ])
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              if (!locationNotifier.isPressed && selectedIndex == 0)
                                TextButton(
                                  onPressed: () async {},
                                  child: Text(AppLocalizations.of(context).translate('introductionpage_next'), style: textStyleAkko16Grey),
                                )
                              // else if (!notificationNotifier.isPressed && selectedIndex == 1)
                              //   TextButton(
                              //     onPressed: () async {},
                              //     child: Text(AppLocalizations.of(context).translate('introductionpage_next'), style: textStyleAkko16Grey),
                              //   )
                              else
                                TextButton(
                                  onPressed: () async {
                                    selectedIndex != _pages.length - 1
                                        ? await _pageController.nextPage(duration: const Duration(milliseconds: 300), curve: Curves.easeIn)
                                        : _goToMenu();
                                  },
                                  child: selectedIndex == _pages.length - 1
                                      ? Text(AppLocalizations.of(context).translate('introductionpage_start'), style: textStyleAkko24Blue)
                                      : Text(AppLocalizations.of(context).translate('introductionpage_next'), style: textStyleAkko16Blue),
                                )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  @override
  void onQuestionComplete(QuestionResultBase answer) {
    print('_LoginPageState: OnQuestionComplete [$answer]');
  }

  @override
  Future<void> onQuestionnaireComplete(Map<int, QuestionResultBase> answers) async {
    print('_LoginPageState: OnQuestionnaireComplete [$answers]');
    await ref.read(questionnaireRepository).submitQuestionnaire(answers.toString());
    _goToMenu();
  }
}
