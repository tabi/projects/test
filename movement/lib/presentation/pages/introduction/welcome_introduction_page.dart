import 'dart:io';

import 'package:background_locator/background_locator.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/svg.dart';

import '../../../color_pallet.dart';
import '../../../infrastructure/services/ios_service.dart';
import '../../../infrastructure/services/localization_service.dart';
import '../../../providers.dart';
import '../../../text_style.dart';
import '../../widgets/elevated_button.dart';

class WelcomeIntroductionPage extends ConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, top: 20, right: 25),
      child: Column(
        children: [
          Row(
            children: [Text(AppLocalizations.of(context).translate('welcomeintroductionpage_title'), style: textStyleSoho24Black)],
          ),
          Expanded(
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.2,
                  width: MediaQuery.of(context).size.width * 0.7,
                  child: SvgPicture.asset('assets/images/2.svg'),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.05,
                ),
                Text(
                  AppLocalizations.of(context).translate('welcomeintroductionpage_welcomemessage'),
                  style: textStyleSoho20Black,
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.01,
                ),
                Column(
                  children: [
                    Text(
                      AppLocalizations.of(context).translate('welcomeintroductionpage_introtext1'),
                      style: textStyleAkko16Black,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.01,
                    ),
                    Text(
                      AppLocalizations.of(context).translate('welcomeintroductionpage_introtext2'),
                      style: textStyleAkko16Black,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.02,
                    ),
                  ],
                ),
                Expanded(
                  child: Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Consumer(
                      builder: (context, ref, child) {
                        final notifier = ref.watch(introLocationNotifierProvider);
                        if (!notifier.isPressed) {
                          return ElevatedButtonWidget(
                            buttonText: AppLocalizations.of(context).translate('introductionpage_locationsharing'),
                            screenWidth: MediaQuery.of(context).size.width,
                            buttonColor: ColorPallet.primaryColor,
                            onPressed: () async {
                              await notifier.askLocationPermission();

                              if (Platform.isIOS) {
                                final data = <String, int>{'countInit': 1};

                                await BackgroundLocator.registerLocationUpdate(
                                  CallbackHandler.iosCallback,
                                  initDataCallback: data,
                                  initCallback: CallbackHandler.init,
                                  iosSettings: const IOSSettings(showsBackgroundLocationIndicator: true),
                                );

                                await BackgroundLocator.isServiceRunning();
                              } else if (Platform.isAndroid) {
                                await ref.read(foregroundServiceProvider).startForegroundServiceAsync();
                              }

                              await notifier.checkPermission();
                            },
                          );
                        } else {
                          if (notifier.isGranted) {
                            return ElevatedButtonWidget(
                              buttonText: AppLocalizations.of(context).translate('introductionpage_locationsharingsucces'),
                              screenWidth: MediaQuery.of(context).size.width,
                              buttonColor: ColorPallet.lightGreen,
                              onPressed: () {},
                            );
                          } else {
                            return ElevatedButtonWidget(
                              buttonText: AppLocalizations.of(context).translate('introductionpage_locationsharingfailed'),
                              screenWidth: MediaQuery.of(context).size.width,
                              buttonColor: ColorPallet.midGray,
                              onPressed: () {},
                            );
                          }
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
