import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import '../../infrastructure/repositories/dtos/enums/log_type.dart';
import '../../infrastructure/repositories/log_repository.dart';
import '../../providers.dart';
import '../pages/calendar_page_adapter.dart';
import '../pages/day_overview_page/day_overview_page.dart';
import '../pages/settings_page.dart';

class MenuWidget extends ConsumerWidget {
  final _pages = <Widget>[CalendarPageAdapter(), DayOverviewPage(), SettingsPage()];

  Future<void> showCompletedDialog(BuildContext context, WidgetRef ref) async {
    final calendarPageNotifier = ref.watch(calendarPageNotifierProvider);
    if ((await calendarPageNotifier.isWeekCompleted) && calendarPageNotifier.hasDisplayedFinale == false) {
      await weekCompletedDialog(context, ref);
      calendarPageNotifier.hasDisplayedFinale = true;
    }
  }

  void syncSensorGeolocations(WidgetRef ref) async {
    try {
      //TODO: should not use a generic try catch here and should move this somewhere else
      await ref.read(sensorRepository).syncSensorGeolocations();
    } catch (error) {
      print(error.toString());
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    log('MenuWidget::build', '', LogType.Flow);
    final menuNotifier = ref.watch(menuNotifierProvider);
    showCompletedDialog(context, ref);
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
      appBar: PreferredSize(preferredSize: const Size.fromHeight(0), child: Container()),
      // floatingActionButton: menuNotifier.shouldDisplayFAB ? const FancyFab() : null,
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      bottomNavigationBar: Consumer( 
        builder: (context, ref, child) {
          return BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Theme.of(context).scaffoldBackgroundColor,
            selectedItemColor: Theme.of(context).primaryColor,
            unselectedItemColor: Theme.of(context).colorScheme.secondary,
            showUnselectedLabels: true,
            currentIndex: menuNotifier.currentPage,
            onTap: (index) {
              menuNotifier.setPage(index);
              syncSensorGeolocations(ref);
            },
            items: const [
              BottomNavigationBarItem(
                label: 'Kalender',
                icon: Icon(Icons.calendar_today_rounded, size: 30),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.location_on_sharp, size: 30),
                label: 'Verplaatsingen',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings, size: 30),
                label: 'Instellingen',
              )
            ],
          );
        },
      ),
      body: PageView.builder(
        controller: menuNotifier.controller,
        itemCount: 3,
        physics: const NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return _pages[menuNotifier.currentPage];
        },
      ),
    );
  }
}
