import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';

import '../../color_pallet.dart';
import '../../infrastructure/notifiers/classified_period_notifier.dart';
import '../../infrastructure/services/localization_service.dart';
import 'date_picker_widget/date_picker_widget.dart';

class TimeDisplayWidget extends ConsumerWidget {
  final ClassifiedPeriodNotifier classifiedPeriodNotifier;

  const TimeDisplayWidget(this.classifiedPeriodNotifier);

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final classifiedPeriod = classifiedPeriodNotifier.classifiedPeriodDto.classifiedPeriod;
    return InkWell(
      onTap: () {
        showModalBottomSheet<void>(
          context: context,
          builder: (context) {
            return DatePickerWidget(
              initialStartDate: classifiedPeriod.startDate,
              initialEndDate: classifiedPeriod.endDate,
              onComplete: (DateTime start, DateTime end) {
                classifiedPeriodNotifier.updateClassifiedPeriod(startDate: start, endDate: end);
              },
            );
          },
        );
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            AppLocalizations.of(context).translate('locationdetailspage_fromtime'),
            style: const TextStyle(color: ColorPallet.darkTextColor),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Text(
              DateFormat('Hm').format(classifiedPeriod.startDate),
              style: const TextStyle(color: Color(0xFFAAAAAA)),
            ),
          ),
          Text(
            ' ${AppLocalizations.of(context).translate('locationdetailspage_totime')} ',
            style: const TextStyle(color: ColorPallet.darkTextColor),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: Text(
              DateFormat('Hm').format(classifiedPeriod.endDate),
              style: const TextStyle(color: Color(0xFFAAAAAA)),
            ),
          ),
        ],
      ),
    );
  }
}
