import 'package:flutter/material.dart';

import '../../text_style.dart';

mixin TextFieldMixin {
  void onTextChanged(
    String source,
    String text,
  );
}

class TextFieldWidget extends StatefulWidget {
  final String hintText;
  final Widget icon;
  final String source;
  final TextInputType keyboardType;
  final bool isObscureText;
  final TextFieldMixin delegate;

  TextFieldWidget({
    required this.hintText,
    required this.icon,
    required this.source,
    required this.keyboardType,
    required this.isObscureText,
    required this.delegate,
  });

  @override
  State<TextFieldWidget> createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  late bool isVisible;

  @override
  void initState() {
    isVisible = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      cursorColor: Colors.white,
      style: textStyleSourceSans16,
      keyboardType: widget.keyboardType,
      obscureText: widget.isObscureText ? isVisible : widget.isObscureText,
      scrollPadding: const EdgeInsets.only(bottom: 600),
      decoration: this.widget.isObscureText
          ? InputDecoration(
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              icon: widget.icon,
              hintText: widget.hintText,
              hintStyle: textStyleAkko16,
              suffixIcon: IconButton(
                icon: Icon(
                  isVisible ? Icons.visibility : Icons.visibility_off,
                  color: Colors.white.withOpacity(0.7),
                ),
                onPressed: () {
                  setState(() {
                    isVisible = isVisible;
                  });
                },
              ),
            )
          : InputDecoration(
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              icon: widget.icon,
              hintText: widget.hintText,
              hintStyle: textStyleAkko16,
            ),
      onChanged: (value) {
        widget.delegate.onTextChanged(widget.source, value);
      },
    );
  }
}
