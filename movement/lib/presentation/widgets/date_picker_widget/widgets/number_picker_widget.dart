import 'package:flutter/cupertino.dart';

import '../util/date_extensions.dart';

class NumberPickerWidget extends StatelessWidget {
  final DateTime selectedDate;
  final bool isHour;
  final DateTime? maxDate;
  final Function(int) onChanged;

  NumberPickerWidget({
    required this.selectedDate,
    required this.onChanged,
    required this.isHour,
    this.maxDate,
  });

  List<int> getPickerValues() {
    List<int> values;
    if (isHour) {
      values = [for (var i = 0; i < 24; i += 1) i];
      if (maxDate != null && selectedDate.isSameDay(maxDate!)) values = values.where((v) => v <= maxDate!.hour).toList();
    } else {
      values = [for (var i = 0; i < 60; i += 1) i];
      if (maxDate != null && selectedDate.isSameDayAndHour(maxDate!)) values = values.where((v) => v <= maxDate!.minute).toList();
    }
    return values;
  }

  @override
  Widget build(BuildContext context) {
    final values = getPickerValues();
    final startValue = isHour ? selectedDate.hour : selectedDate.minute;

    return CupertinoPicker(
      scrollController: FixedExtentScrollController(initialItem: values.indexOf(startValue)),
      magnification: 1.35,
      squeeze: 1,
      itemExtent: 30,
      selectionOverlay: SizedBox(),
      onSelectedItemChanged: (int selectedItem) {
        onChanged(values[selectedItem]);
      },
      children: List<Widget>.generate(
        values.length,
        (int index) {
          return Center(
            child: Text(
              values[index].toString().padLeft(2, '0'),
            ),
          );
        },
      ),
    );
  }
}
