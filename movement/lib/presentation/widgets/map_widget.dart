import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:latlong2/latlong.dart';

import '../../infrastructure/repositories/dtos/movement_dto.dart';
import '../../infrastructure/repositories/dtos/stop_dto.dart';
import '../../providers.dart';
import '../theme/icon_mapper.dart';

LatLng? _cachedCurrentLocation;

class MapWidget extends ConsumerStatefulWidget {
  final List<StopDto> stopDtos;
  final List<MovementDto> movementDtos;
  const MapWidget(this.stopDtos, this.movementDtos);

  @override
  ConsumerState<MapWidget> createState() => _MapWidgetState();
}

class _MapWidgetState extends ConsumerState<MapWidget> {
  final mapController = MapController();
  var markers = <Marker>[];
  var points = <LatLng>[];
  var bounds = LatLngBounds();
  var isDisposed = false;

  Marker _getMarker(LatLng latLng, String? icon) {
    return Marker(
      width: 40,
      height: 40,
      point: latLng,
      builder: (ctx) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(40),
          color: Colors.white,
        ),
        alignment: Alignment.center,
        child: FaIconMapper.getFaIcon(icon),
      ),
    );
  }

  void setupMap() {
    markers = [];
    points = [];
    bounds = LatLngBounds();
    for (final stopDto in widget.stopDtos) {
      if (stopDto.centroid != null) {
        markers.add(_getMarker(stopDto.centroid!, stopDto.reason?.icon));
        bounds.extend(stopDto.centroid!);
      }
    }
    for (final movementDto in widget.movementDtos) {
      movementDto.route.forEach(points.add);
      movementDto.route.forEach(bounds.extend);
    }
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (bounds.isValid) {
        mapController.fitBounds(bounds);
        mapController.move(bounds.center, min(mapController.zoom - 1, 12));
      } else {
        handleEmptyDtos();
      }
    });
  }

  Future<void> handleEmptyDtos() async {
    final currentLatLong = await ref.read(deviceServiceProvider).getCurrentLocationAsync();
    if (currentLatLong != null) {
      await mapController.onReady;
      await Future.delayed(const Duration(seconds: 1), () {}); // Redrawing the map too quickly may result in showing a grey screen for a long time.
      _cachedCurrentLocation = LatLng(currentLatLong[0], currentLatLong[1]);
      if (!isDisposed) {
        bounds.extend(_cachedCurrentLocation);
        mapController.fitBounds(bounds);
        mapController.move(bounds.center, 12);
      }
    }
  }

  @override
  void dispose() {
    isDisposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setupMap();
    isDisposed = false;
    return SizedBox(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: FlutterMap(
        mapController: mapController,
        layers: [
          TileLayerOptions(urlTemplate: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', subdomains: ['a', 'b', 'c']),
          MarkerLayerOptions(markers: markers),
          PolylineLayerOptions(
            polylines: [
              Polyline(
                strokeWidth: 5,
                color: const Color(0xFF00589C),
                borderStrokeWidth: 2,
                borderColor: const Color(0xFF073C6A),
                points: points,
              ),
            ],
          ),
        ],
        options: MapOptions(center: _cachedCurrentLocation ?? LatLng(52.0660149, 4.3987896), zoom: _cachedCurrentLocation == null ? 5 : 12),
      ),
    );
  }
}
