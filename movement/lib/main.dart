import 'dart:isolate';
import 'dart:ui';

import 'package:background_locator/background_locator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'https_certificate_workaround.dart';
import 'infrastructure/notifiers/generic_notifier.dart';
import 'infrastructure/notifiers/loading_notifier.dart';
import 'infrastructure/services/ios_service.dart';
import 'infrastructure/services/localization_service.dart';
import 'presentation/menu/menu_widget.dart';
import 'presentation/pages/login_page.dart';
import 'presentation/theme/theme_widget.dart';
import 'presentation/widgets/app_retain_widget.dart';
import 'providers.dart';

final container = ProviderContainer();

Future<void> main() async {
  await initConfig();
  final port = ReceivePort();
  if (IsolateNameServer.lookupPortByName(CallbackHandler.isolateName) != null) {
    IsolateNameServer.removePortNameMapping(CallbackHandler.isolateName);
  }

  IsolateNameServer.registerPortWithName(port.sendPort, CallbackHandler.isolateName);

  await BackgroundLocator.initialize();
  runApp(
    UncontrolledProviderScope(
      container: container,
      child: MyApp(),
    ),
  );
}

Future<void> initConfig() async {
  WidgetsFlutterBinding.ensureInitialized();
  enableUnverifiedHttpsCertificate();
  final prefs = await SharedPreferences.getInstance();

  final startIntroduction = prefs.getBool('introduction');
  if (startIntroduction == null) {
    await prefs.setBool('introduction', true);
  }

  // final startQuestionnaire = prefs.getBool('questionnaire');
  // if (startQuestionnaire == null) {
  //   await prefs.setBool('questionnaire', true);
  // }
}

class MyApp extends StatelessWidget {
  final notifier = StateNotifierProvider((ref) => LoadingNotifier(ref.watch(database)));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: const [
        Locale('en', ''),
        Locale('nl', ''),
      ],
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      debugShowCheckedModeBanner: false,
      color: Colors.white,
      locale: const Locale('nl'),
      home: ThemeWidget(
        child: AppRetainWidget(
          child: Consumer(builder: (context, ref, child) {
            final state = ref.watch(notifier);
            if (state is Loaded<bool>) {
              if (state.loadedObject) {
                CallbackHandler.startIOSService();
                return MenuWidget();
              } else {
                return LoginPage();
              }
            } else {
              return Container(
                color: Colors.white,
              );
            }
          }),
        ),
      ),
    );
  }
}
