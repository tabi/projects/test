import 'package:flutter/material.dart';

import 'color_pallet.dart';

class AppFonts {
  static const TextStyle xxlBoldSohoPro = TextStyle(color: ColorPallet.darkTextColor, fontSize: 23, fontWeight: FontWeight.w500, fontFamily: 'Soho Pro');
  static const TextStyle grayedOutNormalText = TextStyle(color: ColorPallet.grayTextColor);
}
