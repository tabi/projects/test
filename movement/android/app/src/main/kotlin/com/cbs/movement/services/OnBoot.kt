package com.cbs.movement.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.core.content.ContextCompat.startForegroundService
import com.cbs.movement.MainActivity

class OnBoot : BroadcastReceiver() {

    private val TAG = "OnReceive"

    override fun onReceive(context: Context, intent: Intent) {

        if (Intent.ACTION_BOOT_COMPLETED == intent.action)
        {
            Log.v(TAG, "Start LocationService")

            val serviceIntent = Intent(context, LocationService::class.java)
            startForegroundService(context, serviceIntent)
        }

    }
}